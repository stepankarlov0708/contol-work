from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

engine = create_engine('sqlite:///User.db', echo=True)
Base = declarative_base()


class User(Base):
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True)
    username = Column(String)
    surname = Column(String)
    cash = Column(Integer)
    date = Column(String)

    def __repr__(self):
        return "<User('%s','%s', '%s', '%s')>" % (self.username, self.surname, self.cash, self.date)


def add_user(Session):
    username, surname, cash, date = input(), input(), input(), input()
    user = User(username=username, surname=surname,
                cash=cash, date=date)
    with Session.begin() as session:
        session.add(user)
        print(session.new)
        session.commit()
        print(user.id)


if __name__ == "__main__":
    engine = create_engine('sqlite:///User.db', echo=True)
    Base.metadata.create_all(engine)
    session = sessionmaker(bind=engine)
    with session.begin() as s:
        s.add_all(
            [
                User(username="Petya", surname="Petrov", cash=1544, date="21.01.1996"),
            ]
        )
        s.commit()
